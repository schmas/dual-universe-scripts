function ButtonFactory.createButton(self, screen, x, y, hx, hy, text, fun)
    button = "<div class='bootstrap' style='width:".. (hx*100) ..
             "vw;height:" .. (hy*100) .."vh;overflow:hidden;font-size:".. (hy*50) ..
             "vh;'>" .. text .. "</div>"
    id = screen.addContent(x * 100, y * 100, button)
    self:createButtonArea(id, x, y, hx, hy, text, fun)
    return id
end

function ButtonFactory.customizeButton(self, screen, id, style)
    local area = self.areas[id]
    local button = "<div class='bootstrap' style='width:".. (area.hx*100) ..
             "vw;height:" .. (area.hy*100) .."vh;overflow:hidden;font-size:".. (area.hy*50) ..
             "vh;" .. style .. "'>" .. area.text .. "</div>"
    screen.resetContent(id,button)
end

-- use them:

button2 = ButtonFactory:createButton(screen, 0.1, 0.2, 0.1, 0.09, "test2", 
  function() system.print("Button 2 pressed") 
end)
-- Change the style of the second button
ButtonFactory:customizeButton(screen, button2, 'background-color:#334455;opacity: 0.6;')