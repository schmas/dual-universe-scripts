-- tick(updateDB)
local tier = 3 --export
local ident = "ResourcesT3" --export
local pyriteMax =  166400 * 3 --export
local petaliteMax = 166400 * 3 --export
local garnieriteMax = 166400 * 3 --export
local acanthiteMax = 166400 * 3 --export

local pyriteDensity = 5.04 --export
local petaliteDensity = 1.35 --export
local garnieriteDensity = 1.28 --export
local acanthiteDensity = 2.65 --export

data = {}
data["bereich"] = ident
data["tier"] = tier
dataCont = {}
data["container"] = dataCont
dataCont[1] = {
    name ='Pyrite',
    mass=pyrite.getItemsMass(), 
    max=pyriteMax, 
    density = pyriteDensity }
dataCont[2] = {
    name ='Petalite',
    mass=petalite.getItemsMass(), 
    max=petaliteMax, 
    density = petaliteDensity}
dataCont[3] = {
    name ='Garnierite',
    mass=garnierite.getItemsMass(), 
    max=garnieriteMax, 
    density = garnieriteDensity}
dataCont[4] = {
    name ='Acanthite',
    mass=acanthite.getItemsMass(), 
    max=acanthiteMax, 
    density = acanthiteDensity }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)

-- Tier 1
data = {}
local tier = 1 --export
local ident = "ResourcesT1" --export
local hematiteMax =  166400 * 3 --export
local coalMax = 166400 * 3 --export
local bauxiteMax = 166400 * 3 --export
local quartzMax = 166400 * 3 --export

local hematiteDensity = 5.04 --export
local coalDensity = 1.35 --export
local bauxiteDensity = 1.28 --export
local quartzDensity = 2.65 --export

data = {}
data["bereich"] = ident
data["tier"] = tier
dataCont = {}
data["container"] = dataCont
dataCont[1] = {
    name ='Hematite',
    mass=hematite.getItemsMass(), 
    max=hematiteMax, 
    density=hematiteDensity }
dataCont[2] = {
    name ='Coal',
    mass=coal.getItemsMass(), 
    max=coalMax, 
    density=coalDensity }
dataCont[3] = {
    name ='Bauxite',
    mass=bauxite.getItemsMass(), 
    max=bauxiteMax,
    density=bauxiteDensity  }
dataCont[4] = {
    name ='Quartz',
    mass=quartz.getItemsMass(), 
    max=quartzMax, 
    density=quartzDensity  }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)

--tier 2
data = {}
local tier = 2 --export
local ident = "ResourcesT2" --export
local natronMax =  166400 * 3 --export
local malachiteMax = 166400 * 3 --export
local limestoneMax = 166400 * 3 --export
local chromiteMax = 166400 * 3 --export

local natronDensity = 1.55 --export
local malachiteDensity = 4 --export
local limestoneDensity = 2.71  --export
local chromiteDensity = 4.54 --export

data = {}
data["bereich"] = ident
data["tier"] = tier
dataCont = {}
data["container"] = dataCont
dataCont[1] = {
    name ='Natron',
    mass=natron.getItemsMass(), 
    max=natronMax, 
    density=natronDensity }
dataCont[2] = {
    name ='Malachite',
    mass=malachite.getItemsMass(), 
    max=malachiteMax, 
    density=malachiteDensity }
dataCont[3] = {
    name ='Limestone',
    mass=limestone.getItemsMass(), 
    max=limestoneMax, 
    density=limestoneDensity  }
dataCont[4] = {
    name ='Chromite',
    mass=chromite.getItemsMass(), 
    max=chromiteMax, 
    density=chromiteDensity  }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)