--unit.tick(updateDB)
data = {}
data["bereich"] = ident
dataCont = {}
data["container"] = dataCont

dataCont["Hematite"] = {mass = hematite.getItemsMass(), maxcap = defaultmaxcap, density = denshematite}
dataCont["Coal"] = {mass = coal.getItemsMass(), maxcap = defaultmaxcap, density = denscoal}
dataCont["Bauxite"] = {mass = bauxite.getItemsMass(), maxcap = defaultmaxcap, density = densbauxite}
dataCont["Quartz"] = {mass = quartz.getItemsMass(), maxcap = defaultmaxcap, density = densquartz}

db.setStringValue(ident, json.encode(data))


-- unit. start()
db.clear()
ident = "Rohstoffe"
unit.setTimer("updateDB", 2)
defaultmaxcap = 166400
-- kg / l
denshematite = 5.04
denscoal = 1.35
densbauxite = 1.28
densquartz = 2.65


