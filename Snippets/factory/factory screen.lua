-- backup
{"slots":{"0":{"name":"display","type":{"events":[],"methods":[]}},"1":{"name":"db","type":{"events":[],"methods":[]}},"2":{"name":"dbStatus","type":{"events":[],"methods":[]}},"3":{"name":"dbMach","type":{"events":[],"methods":[]}},"4":{"name":"switch","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"display.clear()\n\nunit.setTimer(\"updateScreenDB\", 1)\n\n","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"0"},{"code":"html = \"<style>table, th, td { border: 1px solid green;} td { text-align: center;} div {font-size:22px;}</style>\"\n\nhtml = html ..\"<div style='font-size:25px'>\"\nhtml = html ..\"<table  style='margin-left: 10px; width: 100%'>\"\nhtml = html ..\"<thead>\"\n  html = html ..\"<tr style='margin-top:150px;'>\"\n \thtml = html ..\"<th style='width:80px; margin-left: 10px;'>Unit</th>\"\n\thtml = html ..\"<th style='margin-left: 250px; width:50px;'>Status</th>\"\n\thtml = html ..\"<th style='margin-left: 20px; width:50px;'>Cycles</th>\"\n\thtml = html ..\"<th style='margin-left: 20px; width:150px;'>Efficiency</th>\"\n  html = html ..\"</tr>\"\nhtml = html ..\"</thead>\"\n\n\nlocal statusEntryKeys = json.decode(dbMach.getKeys())\nfor key, value in ipairs(statusEntryKeys) do\n    entry =  json.decode(dbMach.getStringValue (value))\n    html = html ..\"<tr>\"\n    html = html .. \"<td style='background-color:gray'> \".. value ..\" </td>\"\n    html = html .. \"<td \" \n    if entry[\"status\"] == \"STOPPED\" then\n         html = html .. \"style='background-color: yellow'\" \n    elseif entry[\"status\"] == \"RUNNING\" then\n         html = html .. \"style='background-color: green'\"\n    else \n         html = html .. \"style='background-color: red'\"\n    end\n    html = html .. \"> \".. entry[\"status\"] ..\" </td>\"\n    html = html .. \"<td> \".. entry[\"cycle\"] ..\" </td>\"\n    html = html .. \"<td> \".. entry[\"efficiency\"] ..\" </td>\"\n\n    html = html ..\"</tr>\"\nend\nhtml = html ..\"</table>\"\nhtml = html ..\"</div>\"\n\ndisplay.setHTML(html)","filter":{"args":[{"value":"updateScreenDB"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"1"},{"code":"dbMach.clear()\nswitch.activate()","filter":{"args":[],"signature":"start()","slotKey":"-2"},"key":"2"},{"code":"switch.deactivate()","filter":{"args":[],"signature":"stop()","slotKey":"-2"},"key":4}],"methods":[],"events":[]}
{"slots":{"0":{"name":"display","type":{"events":[],"methods":[]}},"1":{"name":"db","type":{"events":[],"methods":[]}},"2":{"name":"dbStatus","type":{"events":[],"methods":[]}},"3":{"name":"dbMach","type":{"events":[],"methods":[]}},"4":{"name":"switch","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"display.clear()\n\nunit.setTimer(\"updateScreenDB\", 1)\n\n","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"0"},{"code":"html = \"<style>table, th, td { border: 1px solid green;} td { text-align: center;} div {font-size:22px;}</style>\"\n\nhtml = html ..\"<div style='font-size:25px'>\"\nhtml = html ..\"<table  style='margin-left: 10px; width: 100%'>\"\nhtml = html ..\"<thead>\"\n  html = html ..\"<tr style='margin-top:150px;'>\"\n \thtml = html ..\"<th style='width:80px; margin-left: 10px;'>Unit</th>\"\n\thtml = html ..\"<th style='margin-left: 250px; width:50px;'>Status</th>\"\n\thtml = html ..\"<th style='margin-left: 20px; width:50px;'>Cycles</th>\"\n\thtml = html ..\"<th style='margin-left: 20px; width:150px;'>Efficiency</th>\"\n  html = html ..\"</tr>\"\nhtml = html ..\"</thead>\"\n\n\nlocal statusEntryKeys = json.decode(dbMach.getKeys())\nfor key, value in ipairs(statusEntryKeys) do\n    entry =  json.decode(dbMach.getStringValue (value))\n    html = html ..\"<tr>\"\n    html = html .. \"<td style='background-color:gray'> \".. value ..\" </td>\"\n    html = html .. \"<td \" \n    if entry[\"status\"] == \"STOPPED\" then\n         html = html .. \"style='background-color: yellow'\" \n    elseif entry[\"status\"] == \"RUNNING\" then\n         html = html .. \"style='background-color: green'\"\n    else \n         html = html .. \"style='background-color: red'\"\n    end\n    html = html .. \"> \".. entry[\"status\"] ..\" </td>\"\n    html = html .. \"<td> \".. entry[\"cycle\"] ..\" </td>\"\n    html = html .. \"<td> \".. entry[\"efficiency\"] ..\" </td>\"\n\n    html = html ..\"</tr>\"\nend\nhtml = html ..\"</table>\"\nhtml = html ..\"</div>\"\n\ndisplay.setHTML(html)","filter":{"args":[{"value":"updateScreenDB"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"1"},{"code":"dbMach.clear()\nswitch.activate()","filter":{"args":[],"signature":"start()","slotKey":"-2"},"key":"2"},{"code":"switch.deactivate()","filter":{"args":[],"signature":"stop()","slotKey":"-2"},"key":"3"}],"methods":[],"events":[]}

--second
{"slots":{"0":{"name":"container","type":{"events":[],"methods":[]}},"1":{"name":"screen","type":{"events":[],"methods":[]}},"2":{"name":"smelter_1","type":{"events":[],"methods":[]}},"3":{"name":"smelter_alu_1","type":{"events":[],"methods":[]}},"4":{"name":"smelter_alu_2","type":{"events":[],"methods":[]}},"5":{"name":"chem_1","type":{"events":[],"methods":[]}},"6":{"name":"db","type":{"events":[],"methods":[]}},"7":{"name":"dbStatus","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":" sgui:process(x,y)","filter":{"args":[{"variable":"*"},{"variable":"*"}],"signature":"mouseDown(x,y)","slotKey":"1"},"key":"0"},{"code":"ganeriteFertig()","filter":{"args":[],"signature":"completed()","slotKey":"2"},"key":"1"},{"code":"aluFertig()","filter":{"args":[],"signature":"completed()","slotKey":"3"},"key":"2"},{"code":"aluFertig()","filter":{"args":[],"signature":"completed()","slotKey":"4"},"key":"3"},{"code":"html = \"<H1>HELLO WORLD</H1>\"\n-- create a button handler\n\n--screen.setHTML(html)\n--ui = sgui.new()\nlocal buttonId = sgui:createButtonArea(screen, 0.1, 0.1, 0.1, 0.1, \"label\", test)\n ","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"4"},{"code":"function test()\n   return \"<div>CLICKED</div>\"\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"5"}],"methods":[],"events":[]}
-- unit.start
unit.setTimer("updateScreenDB", 1)


-- unit.tick(updateScreenDB) 
-- this db refers to the same db as factory data producing
html = "<style>table, th, td { border: 1px solid green;} td { text-align: center;} div {font-size:22px;}</style>"

html = html .."<div style='font-size:25px'>"
html = html .."<table  style='margin-left: 10px; width: 100%'>"
html = html .."<thead>"
  html = html .."<tr style='margin-top:150px;'>"
 	html = html .."<th style='width:80px; margin-left: 10px;'>Unit</th>"
	html = html .."<th style='margin-left: 250px; width:50px;'>Status</th>"
	html = html .."<th style='margin-left: 20px; width:50px;'>Cycles</th>"
	html = html .."<th style='margin-left: 20px; width:150px;'>Efficiency</th>"
  html = html .."</tr>"
html = html .."</thead>"


local statusEntryKeys = json.decode(dbMach.getKeys())
for key, value in ipairs(statusEntryKeys) do
    entry =  json.decode(dbMach.getStringValue (value))
    html = html .."<tr>"
    html = html .. "<td> ".. value .." </td>"
    html = html .. "<td " 
    if entry["status"] == "STOPPED" then
        html = html .. "style='background-color: yellow'" 
    elseif entry["status"] == "RUNNING" then
         html = html .. "style='background-color: green'"
    else 
         html = html .. "style='background-color: red'"
    end
    html = html .. "> ".. entry["status"] .." </td>"
    html = html .. "<td> ".. entry["cycle"] .." </td>"
    html = html .. "<td> ".. entry["efficiency"] .." </td>"

    html = html .."</tr>"
end
html = html .."</table>"
html = html .."</div>"


display.setHTML(html)


-- system.stop
switch.deactivate()

-- system.start
dbMach.clear()
switch.activate()









