-- backup data producer
{"slots":{"0":{"name":"button","type":{"events":[],"methods":[]}},"1":{"name":"mach1","type":{"events":[],"methods":[]}},"2":{"name":"mach2","type":{"events":[],"methods":[]}},"3":{"name":"mach3","type":{"events":[],"methods":[]}},"4":{"name":"mach4","type":{"events":[],"methods":[]}},"5":{"name":"mach5","type":{"events":[],"methods":[]}},"6":{"name":"db","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"restartAll()","filter":{"args":[],"signature":"pressed()","slotKey":"0"},"key":"0"},{"code":"\nunit.setTimer(\"check\",5)\nrunning = false\nunitname = \"electro_1\"\nmachines = {}\nmachines [1] = mach1\nmachines [2] = mach2\nmachines [3] = mach3\nmachines [4] = mach4\nmachines [5] = mach5\n\nfunction restartAll()\n    if running == false then\n    \trunning = true\n    \tsoft()\n     else print(\"already running\")\n \tend\nend\n\nfunction soft() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.softStop()\n     end\n     unit.setTimer(\"softtimer\", 120)\nend\n\n\nfunction hard() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.hardStop()\n     end\n     unit.setTimer(\"hardtimer\", 5)\n     unit.stopTimer(\"softtimer\")\nend\n\nfunction restart() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.startAndMaintain()\n     end\n     unit.stopTimer(\"hardtimer\")\n\trunning = false\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"hard()\n","filter":{"args":[{"value":"softtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"},{"code":"restart()\n\n","filter":{"args":[{"value":"hardtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"3"},{"code":"for i, machine in ipairs(machines) do\n\n   local tablenameCycle  = unitname ..\"_\" .. i \n\n    data = {}\n    data[\"status\"] = machine.getStatus()\n    data[\"cycle\"] = machine.getCycleCountSinceStartup()\n    data[\"efficiency\"] = machine.getEfficiency()\n    \n   --daten = json.encode( '[{\"unit\": \"'..tablenameCycle..'\",\"cycle\": \"'..machine.getCycleCountSinceStartup()..'\", \"status\": \"'..machine.getStatus()..'\",\"efficiency\": \"'..machine.getEfficiency()..'\"  }]')\n    \n    db.setStringValue(tablenameCycle, json.encode(data))\nend\n","filter":{"args":[{"value":"check"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"4"},{"code":"for i, machine in ipairs(machines) do\n local tablenameCycle  = unitname ..\"_\" .. i \n    db.setStringValue(tablenameCycle, \"---\")\n    \nend","filter":{"args":[],"signature":"stop()","slotKey":"-1"},"key":"5"}],"methods":[],"events":[]}

{"slots":{"0":{"name":"button","type":{"events":[],"methods":[]}},"1":{"name":"mach1","type":{"events":[],"methods":[]}},"2":{"name":"mach2","type":{"events":[],"methods":[]}},"3":{"name":"mach3","type":{"events":[],"methods":[]}},"4":{"name":"mach4","type":{"events":[],"methods":[]}},"5":{"name":"mach5","type":{"events":[],"methods":[]}},"6":{"name":"db","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"restartAll()","filter":{"args":[],"signature":"pressed()","slotKey":"0"},"key":"0"},{"code":"\nunit.setTimer(\"check\",5)\nrunning = false\nunitname = \"electro_1\" -- has to be unique!\nmachines = {}\nmachines [1] = mach1\nmachines [2] = mach2\nmachines [3] = mach3\nmachines [4] = mach4\nmachines [5] = mach5\n\nfunction restartAll()\n    if running == false then\n    \trunning = true\n    \tsoft()\n     else print(\"already running\")\n \tend\nend\n\nfunction soft() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.softStop()\n     end\n     unit.setTimer(\"softtimer\", 20)\nend\n\n\nfunction hard() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.hardStop()\n     end\n     unit.setTimer(\"hardtimer\", 5)\n     unit.stopTimer(\"softtimer\")\nend\n\nfunction restart() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.startAndMaintain()\n     end\n     unit.stopTimer(\"hardtimer\")\n\trunning = false\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"hard()\n","filter":{"args":[{"value":"softtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"},{"code":"restart()\n\n","filter":{"args":[{"value":"hardtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"3"},{"code":"for i, machine in ipairs(machines) do\n\n   local tablenameCycle  = unitname ..\"_\" .. i \n\n    data = {}\n    data[\"status\"] = machine.getStatus()\n    data[\"cycle\"] = machine.getCycleCountSinceStartup()\n    data[\"efficiency\"] = machine.getEfficiency()\n    \n   --daten = json.encode( '[{\"unit\": \"'..tablenameCycle..'\",\"cycle\": \"'..machine.getCycleCountSinceStartup()..'\", \"status\": \"'..machine.getStatus()..'\",\"efficiency\": \"'..machine.getEfficiency()..'\"  }]')\n    \n    db.setStringValue(tablenameCycle, json.encode(data))\nend\n","filter":{"args":[{"value":"check"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"4"},{"code":"for i, machine in ipairs(machines) do\n local tablenameCycle  = unitname ..\"_\" .. i \n    db.setStringValue(tablenameCycle, \"---\")\n    \nend","filter":{"args":[],"signature":"stop()","slotKey":"-1"},"key":"5"}],"methods":[],"events":[]}    

-- data producer
-- needs to be a Programming Board, Slot 1: Button, Slot 2-6 Machines mach1 - mach5, Slot 7 db
-- ! IMPORTANT ! give your unit a name - set unitname in unit.start to an unique name
--unit.start

unit.setTimer("check",5)
running = false
unitname = "electro_1"
machines = {}
machines [1] = mach1
machines [2] = mach2
machines [3] = mach3
machines [4] = mach4
machines [5] = mach5

function restartAll()
    if running == false then
    	running = true
    	soft()
     else print("already running")
 	end
end

function soft() 
	for i, machine in ipairs(machines) do
    	machine.softStop()
     end
     unit.setTimer("softtimer", 120)
end


function hard() 
	for i, machine in ipairs(machines) do
    	machine.hardStop()
     end
     unit.setTimer("hardtimer", 5)
     unit.stopTimer("softtimer")
end

function restart() 
	for i, machine in ipairs(machines) do
    	machine.startAndMaintain()
     end
     unit.stopTimer("hardtimer")
	running = false
end

-- unit.stop
for i, machine in ipairs(machines) do
  local tablenameCycle  = unitname .."_" .. i 
     db.setStringValue(tablenameCycle, "---")
     
 end

 -- unit.check
 for i, machine in ipairs(machines) do

  local tablenameCycle  = unitname .."_" .. i 

   data = {}
   data["status"] = machine.getStatus()
   data["cycle"] = machine.getCycleCountSinceStartup()
   data["efficiency"] = machine.getEfficiency()
   
  --daten = json.encode( '[{"unit": "'..tablenameCycle..'","cycle": "'..machine.getCycleCountSinceStartup()..'", "status": "'..machine.getStatus()..'","efficiency": "'..machine.getEfficiency()..'"  }]')
   
   db.setStringValue(tablenameCycle, json.encode(data))
end


-- unit.hardtimer
restart()


-- unit.softtimer
hard()

-- button.pressed
restartAll()