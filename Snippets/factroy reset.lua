-- Factory Reset: backup
{"slots":{"0":{"name":"button","type":{"events":[],"methods":[]}},"1":{"name":"mach1","type":{"events":[],"methods":[]}},"2":{"name":"mach2","type":{"events":[],"methods":[]}},"3":{"name":"mach3","type":{"events":[],"methods":[]}},"4":{"name":"mach4","type":{"events":[],"methods":[]}},"5":{"name":"mach5","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"restartAll()","filter":{"args":[],"signature":"pressed()","slotKey":"0"},"key":"0"},{"code":"running = false\n\nmachines = {}\nmachines [1] = mach1\nmachines [2] = mach2\nmachines [3] = mach3\nmachines [4] = mach4\nmachines [5] = mach5\n\nfunction restartAll()\n    if running == false then\n    \trunning = true\n    \tsoft()\n     else print(\"already running\")\n \tend\nend\n\nfunction soft() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.softStop()\n     end\n     unit.setTimer(\"softtimer\", 12)\nend\n\n\nfunction hard() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.hardStop()\n     end\n     unit.setTimer(\"hardtimer\", 5)\n     unit.stopTimer(\"softtimer\")\nend\n\nfunction restart() \n\tfor i, machine in ipairs(machines) do\n    \tmachine.startAndMaintain()\n     end\n     unit.stopTimer(\"hardtimer\")\n\trunning = false\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"hard()\n","filter":{"args":[{"value":"softtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"},{"code":"restart()\n\n","filter":{"args":[{"value":"hardtimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"3"}],"methods":[],"events":[]}

--Factory Reset: Lua

-- unit.start()
running = false

machines = {}
machines [1] = mach1
machines [2] = mach2
machines [3] = mach3
machines [4] = mach4
machines [5] = mach5

function restartAll()
    if running == false then
    	running = true
    	soft()
     else print("already running")
 	end
end

function soft() 
	for i, machine in ipairs(machines) do
    	machine.softStop()
     end
     unit.setTimer("softtimer", 120)
end


function hard() 
	for i, machine in ipairs(machines) do
    	machine.hardStop()
     end
     unit.setTimer("hardtimer", 5)
     unit.stopTimer("softtimer")
end

function restart() 
	for i, machine in ipairs(machines) do
    	machine.startAndMaintain()
     end
     unit.stopTimer("hardtimer")
	running = false
end

-- unit.softtimer
hard()
-- unit.softtimer
restart()

-- button
restartAll()


-- status


html = html .. "<tr> <td>" .. value .. "</td>"
local warning_eff = ""
    if dbMach.getFloatValue(value .. "_efficiency") < 0.5 then warning_eff = "background-color: red;" end
html = html .. "<td style='".. warning_eff .."'>" .. dbMach.getFloatValue(value .. "_efficiency").. "</td></tr>"

html = html .. "<td >" .. dbMach.getIntValue(value .. "_cycles").. "</td></tr>"

 local warning = ""
    if dbMach.getStringValue(value .. "_status") ~= 'RUNNING' then warning = "background-color: red;" end
html = html .. "<td style='".. warning .."'>" .. dbMach.getStringValue(value .. "_status").. "</td></tr>"