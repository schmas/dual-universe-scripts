--T1
{"slots":{"0":{"name":"quartz","type":{"events":[],"methods":[]}},"1":{"name":"bauxite","type":{"events":[],"methods":[]}},"2":{"name":"coal","type":{"events":[],"methods":[]}},"3":{"name":"hermatite","type":{"events":[],"methods":[]}},"4":{"name":"screen","type":{"events":[],"methods":[]}},"5":{"name":"screen","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"switch.deactivate()\nscreen.clear()","filter":{"args":[],"signature":"stop()","slotKey":"-1"},"key":"0"},{"code":"unit.setTimer(\"Live\",1)\nswitch.activate()","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"  \nfunction round(number,decimals)\n    local power = 10^decimals\n    return math.floor((number/1000) * power) / power\nend \n\nfunction oreStatus(percent)\n    if percent <= 25 then return \"<th style=\\\"color: red;\\\">LOW</th>\"\n    elseif percent > 25 and percent < 50 then return \"<th style=\\\"color: orange;\\\">LOW</th>\"\n    else return \"<th style=\\\"color: green;\\\">GOOD</th>\"\n    end \nend\n\nlocal useL = 1 \nif useL == 1 then\n    unitMeasure = \"KL\"\nelseif useL == 0 then\n    unitMeasure = \"T\"\nend\n\n\nlocal maxBauxite = 333200\nlocal weightBauxite = 1.28 \nif useL == 1 then\n    massBauxite = round(math.ceil(bauxite.getItemsMass()/weightBauxite),1)\nelseif useL == 0 then  \n    massBauxite = round(math.ceil(bauxite.getItemsMass()),1)\nend\nlocal percentBauxite = math.ceil(((math.ceil((bauxite.getItemsMass()/weightBauxite) - 0.5)/maxBauxite)*100))\nlocal statusBauxite = oreStatus(percentBauxite)\n\n    \n\nlocal maxCoal = 333200\nlocal weightCoal = 1.35 \nif useL == 1 then\n\tmassCoal = round(math.ceil(coal.getItemsMass()/weightCoal),1)\nelseif useL == 0 then\n   \tmassCoal = round(math.ceil(coal.getItemsMass()),1)\nend\nlocal percentCoal = math.ceil(((math.ceil((coal.getItemsMass()/weightCoal) - 0.5)/maxCoal)*100))\nlocal statusCoal = oreStatus(percentCoal)\n\n\nlocal maxHermatite = 999600\nlocal weightHermatite = 5.04 \nif useL == 1 then\n    massHermatite = round(math.ceil(hermatite.getItemsMass()/weightHermatite),1)\nelseif useL == 0 then\n    massHermatite = round(math.ceil(hermatite.getItemsMass()),1)\nend\nlocal percentHermatite = math.ceil(((math.ceil((hermatite.getItemsMass()/weightHermatite) - 0.5)/maxHermatite)*100))\nlocal statusHermatite = oreStatus(percentHermatite)\n\n\nlocal maxQuartz = 333200\nlocal weightQuartz = 2.65 \nif useL == 1 then\n    massQuartz = round(math.ceil(quartz.getItemsMass()/weightQuartz),1)\nelseif useL == 0 then\n    massQuartz = round(math.ceil(quartz.getItemsMass()),1)\nend\n\nlocal percentQuartz = math.ceil(((math.ceil((quartz.getItemsMass()/weightQuartz) - 0.5)/maxQuartz)*100))\nlocal statusQuartz = oreStatus(percentQuartz)\n\n\nhtml = [[\n<style>\nbody {\n  background-image: url('assets.prod.novaquark.com/58139/a0a98231-07bc-4577-bbc6-2f6b361cc4e1.jpg');\n  background-repeat: no-repeat; \n  background-attachment: fixed;\n  background-position: center top;\n}\n</style>\n<div class=\"bootstrap\">\n<h1 style=\"\n\tfont-size: 8em;\n\">Hadez Coperation</h1>\n<table \nstyle=\"\n\tmargin-top: 10px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\twidth: 80%;\n\tfont-size: 4em;\n\">\n\t</br>\n\t<tr style=\"\n\t\twidth: 100%;\n\t\tmargin-bottom: 30px;\n\t\tbackground-color: red;\n\t\tcolor: white;\n\t\">\n\t\t<th>Ore</th>\n\t\t<th>Qty</th>\n\t\t<th>Levels</th>\n\t\t<th>Status</th>\n\t<tr>\n\t\t<th>Bauxite</th>\n\t\t<th>]]..massBauxite..unitMeasure..[[</th>\n\t\t<th>]]..percentBauxite..[[%</th>\n\t\t]]..statusBauxite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Coal</th>\n\t\t<th>]]..massCoal..unitMeasure..[[</th>\n\t\t<th>]]..percentCoal..[[%</th>\n\t\t]]..statusCoal..[[\n\t</tr>\n\t<tr>\n\t\t<th>Hermatite</th>\n\t\t<th>]]..massHermatite..unitMeasure..[[</th>\n\t\t<th>]]..percentHermatite..[[%</th>\n\t\t]]..statusHermatite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Quartz</th>\n\t\t<th>]]..massQuartz..unitMeasure..[[</th>\n\t\t<th>]]..percentQuartz..[[%</th>\n\t\t]]..statusQuartz..[[\n\t</tr>\n\n</table>\n</div>\n]]\n\nscreen.setHTML(html)","filter":{"args":[{"value":"Live"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"}],"methods":[],"events":[]}

--T2
{"slots":{"0":{"name":"screen","type":{"events":[],"methods":[]}},"1":{"name":"chromite","type":{"events":[],"methods":[]}},"2":{"name":"limestone","type":{"events":[],"methods":[]}},"3":{"name":"malachite","type":{"events":[],"methods":[]}},"4":{"name":"natron","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"unit.setTimer(\"Live\",1)","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"0"},{"code":"function round(number,decimals)\n    local power = 10^decimals\n    return math.floor((number/1000) * power) / power\nend \n\nfunction oreStatus(percent)\n    if percent <= 25 then return \"<th style=\\\"color: red;\\\">LOW</th>\"\n    elseif percent > 25 and percent < 50 then return \"<th style=\\\"color: orange;\\\">LOW</th>\"\n    else return \"<th style=\\\"color: green;\\\">GOOD</th>\"\n    end \nend\n\nlocal useL = 1 --export: Use 1 for L or 0 for t\nif useL == 1 then\n    unitMeasure = \"KL\"\nelseif useL == 0 then\n    unitMeasure = \"T\"\nend\n\n-- Chromite Variables \nlocal maxChromite = 166600 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightChromite = 4.54 --export:\nif useL == 1 then\n    massChromite = round(math.ceil(chromite.getItemsMass()/weightChromite),1)\nelseif useL == 0 then\n    massChromite = round(math.ceil(chromite.getItemsMass()),1)\nend\nlocal percentChromite = math.ceil(((math.ceil((chromite.getItemsMass()/weightChromite) - 0.5)/maxChromite)*100))\nlocal statusChromite = oreStatus(percentChromite)\n\n    \n-- Limestone Variables\nlocal maxLimestone = 166600 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightLimestone = 2.71 --export:\nif useL == 1 then\n    massLimestone = round(math.ceil(limestone.getItemsMass()/weightLimestone),1)\nelseif useL == 0 then\n    massLimestone = round(math.ceil(limestone.getItemsMass()),1)\nend\nlocal percentLimestone = math.ceil(((math.ceil((limestone.getItemsMass()/weightLimestone) - 0.5)/maxLimestone)*100))\nlocal statusLimestone = oreStatus(percentLimestone)\n\n-- Malachite Variables\nlocal maxMalachite = 166600 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightMalachite = 4.00 --export:\nif useL == 1 then\n    massMalachite = round(math.ceil(malachite.getItemsMass()/weightMalachite),1)\nelseif useL == 0 then\n    massMalachite = round(math.ceil(malachite.getItemsMass()),1)\nend\nlocal percentMalachite = math.ceil(((math.ceil((malachite.getItemsMass()/weightMalachite) - 0.5)/maxMalachite)*100))\nlocal statusMalachite = oreStatus(percentMalachite)\n\n-- Natron Variables\nlocal maxNatron = 166600 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightNatron = 1.55 --export:\nif useL == 1 then\n    massNatron = round(math.ceil(natron.getItemsMass()/weightNatron),1)\nelseif useL == 0 then\n    massNatron = round(math.ceil(natron.getItemsMass()),1)\nend\nlocal percentNatron = math.ceil(((math.ceil((natron.getItemsMass()/weightNatron) - 0.5)/maxNatron)*100))\nlocal statusNatron = oreStatus(percentNatron)\n\n\nhtml = [[\n<style>\nbody {\n  background-image: url('assets.prod.novaquark.com/58139/a0a98231-07bc-4577-bbc6-2f6b361cc4e1.jpg');\n  background-repeat: no-repeat; \n  background-attachment: fixed;\n  background-position: center top;\n}\n</style>\n<div class=\"bootstrap\">\n<h1 style=\"\n\tfont-size: 8em;\n\">Hadez Coperation</h1>\n<table \nstyle=\"\n\tmargin-top: 10px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\twidth: 80%;\n\tfont-size: 4em;\n\">\n\t</br>\n\t<tr style=\"\n\t\twidth: 100%;\n\t\tmargin-bottom: 30px;\n\t\tbackground-color: red;\n\t\tcolor: white;\n\t\">\n\t\t<th>Ore</th>\n\t\t<th>Qty</th>\n\t\t<th>Levels</th>\n\t\t<th>Status</th>\n\t<tr>\n\t\t<th>Chromite</th>\n\t\t<th>]]..massChromite..unitMeasure..[[</th>\n\t\t<th>]]..percentChromite..[[%</th>\n\t\t]]..statusChromite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Limestone</th>\n\t\t<th>]]..massLimestone..unitMeasure..[[</th>\n\t\t<th>]]..percentLimestone..[[%</th>\n\t\t]]..statusLimestone..[[\n\t</tr>\n\t<tr>\n\t\t<th>Malachite</th>\n\t\t<th>]]..massMalachite..unitMeasure..[[</th>\n\t\t<th>]]..percentMalachite..[[%</th>\n\t\t]]..statusMalachite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Natron</th>\n\t\t<th>]]..massNatron..unitMeasure..[[</th>\n\t\t<th>]]..percentNatron..[[%</th>\n\t\t]]..statusNatron..[[\n\t</tr>\n\n</table>\n</div>\n]]\n\nscreen.setHTML(html)","filter":{"args":[{"value":"Live"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"1"},{"code":"screen.clear()","filter":{"args":[],"signature":"stop()","slotKey":"-1"},"key":"2"}],"methods":[],"events":[]}

--T3
{"slots":{"0":{"name":"screen","type":{"events":[],"methods":[]}},"1":{"name":"acanthite","type":{"events":[],"methods":[]}},"2":{"name":"garnierite","type":{"events":[],"methods":[]}},"3":{"name":"petalite","type":{"events":[],"methods":[]}},"4":{"name":"pyrite","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"unit.setTimer(\"Live\",1)","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"0"},{"code":"function round(number,decimals)\n    local power = 10^decimals\n    return math.floor((number/1000) * power) / power\nend \n\nfunction oreStatus(percent)\n    if percent <= 25 then return \"<th style=\\\"color: red;\\\">LOW</th>\"\n    elseif percent > 25 and percent < 50 then return \"<th style=\\\"color: orange;\\\">LOW</th>\"\n    else return \"<th style=\\\"color: green;\\\">GOOD</th>\"\n    end \nend\n\nlocal useL = 1 --export: Use 1 for L or 0 for t\nif useL == 1 then\n    unitMeasure = \"KL\"\nelseif useL == 0 then\n    unitMeasure = \"T\"\nend\n    \n-- Acanthite Variables \nlocal maxAcanthite = 10400 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightAcanthite = 7.2 --export:\nif useL == 1 then\n    massAcanthite = round(math.ceil(acanthite.getItemsMass()/weightAcanthite),1)\nelseif useL == 0 then\n    massAcanthite = round(math.ceil(acanthite.getItemsMass()),1)\nend\nlocal percentAcanthite = math.ceil(((math.ceil((acanthite.getItemsMass()/weightAcanthite) - 0.5)/maxAcanthite)*100))\nlocal statusAcanthite = oreStatus(percentAcanthite)\n    \n-- Garnierite Variables\nlocal maxGarnierite = 10400 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightGarnierite = 2.6 --export:\nif useL == 1 then\n    massGarnierite = round(math.ceil(garnierite.getItemsMass()/weightGarnierite),1)\nelseif useL == 0 then\n    massGarnierite = round(math.ceil(garnierite.getItemsMass()),1)\nend\nlocal percentGarnierite = math.ceil(((math.ceil((garnierite.getItemsMass()/weightGarnierite) - 0.5)/maxGarnierite)*100))\nlocal statusGarnierite = oreStatus(percentGarnierite)\n\n-- Petalite Variables\nlocal maxPetalite = 10400 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightPetalite = 2.41 --export:\nif useL == 1 then\n    massPetalite = round(math.ceil(petalite.getItemsMass()/weightPetalite),1)\nelseif useL == 0 then\n    massPetalite = round(math.ceil(petalite.getItemsMass()),1)\nend\nlocal percentPetalite = math.ceil(((math.ceil((petalite.getItemsMass()/weightPetalite) - 0.5)/maxPetalite)*100))\nlocal statusPetalite = oreStatus(percentPetalite)\n\n-- Pyrite Variables\nlocal maxPyrite = 10400 --export: This is the maximum mass allowed in container. Update as needed\nlocal weightPyrite = 5.01 --export:\nif useL == 1 then\n    massPyrite = round(math.ceil(pyrite.getItemsMass()/weightPyrite),1)\nelseif useL == 0 then\n    massPyrite = round(math.ceil(pyrite.getItemsMass()),1)\nend\nlocal percentPyrite = math.ceil(((math.ceil((pyrite.getItemsMass()/weightPyrite) - 0.5)/maxPyrite)*100))\nlocal statusPyrite = oreStatus(percentPyrite)\n\n\nhtml = [[\n<style>\nbody { \n  background-image: url('assets.prod.novaquark.com/58139/a0a98231-07bc-4577-bbc6-2f6b361cc4e1.jpg');\n  background-repeat: no-repeat;\n  background-attachment: fixed;\n  background-position: center top; \n</style>\n</style>\n<div class=\"bootstrap\">\n<h1 style=\"\n\tfont-size: 8em;\n\">Hadez Coperation </h1>\n<table \nstyle=\"\n\tmargin-top: 10px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\twidth: 80%;\n\tfont-size: 4em;\n\">\n\t</br>\n\t<tr style=\"\n\t\twidth: 100%;\n\t\tmargin-bottom: 30px;\n\t\tbackground-color: red; \n\t\tcolor: white;\n\t\">\n\t\t<th>Ore</th>\n\t\t<th>Qty</th>\n\t\t<th>Levels</th>\n\t\t<th>Status</th>\n\t<tr>\n\t\t<th>Acanthite</th>\n\t\t<th>]]..massAcanthite..unitMeasure..[[</th>\n\t\t<th>]]..percentAcanthite..[[%</th>\n\t\t]]..statusAcanthite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Garnierite</th>\n\t\t<th>]]..massGarnierite..unitMeasure..[[</th>\n\t\t<th>]]..percentGarnierite..[[%</th>\n\t\t]]..statusGarnierite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Petalite</th>\n\t\t<th>]]..massPetalite..unitMeasure..[[</th>\n\t\t<th>]]..percentPetalite..[[%</th>\n\t\t]]..statusPetalite..[[\n\t</tr>\n\t<tr>\n\t\t<th>Pyrite</th>\n\t\t<th>]]..massPyrite..unitMeasure..[[</th>\n\t\t<th>]]..percentPyrite..[[%</th>\n\t\t]]..statusPyrite..[[\n\t</tr>\n\n</table>\n</div>\n]]\n\nscreen.setHTML(html)","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"screen.clear()","filter":{"args":[],"signature":"stop()","slotKey":"-1"},"key":"2"}],"methods":[],"events":[]}


function round(number,decimals)
    local power = 10^decimals
    return math.floor((number/1000) * power) / power
end 

function oreStatus(percent)
    if percent <= 25 then return "<th style=\"color: red;\">LOW</th>"
    elseif percent > 25 and percent < 50 then return "<th style=\"color: orange;\">LOW</th>"
    else return "<th style=\"color: green;\">GOOD</th>"
    end 
end

local useL = 1 --export: Use 1 for L or 0 for t
if useL == 1 then
    unitMeasure = "KL"
elseif useL == 0 then
    unitMeasure = "T"
end

-- Chromite Variables 
local maxChromite = 166600 --export: This is the maximum mass allowed in container. Update as needed
local weightChromite = 4.54 --export:
if useL == 1 then
    massChromite = round(math.ceil(chromite.getItemsMass()/weightChromite),1)
elseif useL == 0 then
    massChromite = round(math.ceil(chromite.getItemsMass()),1)
end
local percentChromite = math.ceil(((math.ceil((chromite.getItemsMass()/weightChromite) - 0.5)/maxChromite)*100))
local statusChromite = oreStatus(percentChromite)

    
-- Limestone Variables
local maxLimestone = 166600 --export: This is the maximum mass allowed in container. Update as needed
local weightLimestone = 2.71 --export:
if useL == 1 then
    massLimestone = round(math.ceil(limestone.getItemsMass()/weightLimestone),1)
elseif useL == 0 then
    massLimestone = round(math.ceil(limestone.getItemsMass()),1)
end
local percentLimestone = math.ceil(((math.ceil((limestone.getItemsMass()/weightLimestone) - 0.5)/maxLimestone)*100))
local statusLimestone = oreStatus(percentLimestone)

-- Malachite Variables
local maxMalachite = 166600 --export: This is the maximum mass allowed in container. Update as needed
local weightMalachite = 4.00 --export:
if useL == 1 then
    massMalachite = round(math.ceil(malachite.getItemsMass()/weightMalachite),1)
elseif useL == 0 then
    massMalachite = round(math.ceil(malachite.getItemsMass()),1)
end
local percentMalachite = math.ceil(((math.ceil((malachite.getItemsMass()/weightMalachite) - 0.5)/maxMalachite)*100))
local statusMalachite = oreStatus(percentMalachite)

-- Natron Variables
local maxNatron = 166600 --export: This is the maximum mass allowed in container. Update as needed
local weightNatron = 1.55 --export:
if useL == 1 then
    massNatron = round(math.ceil(natron.getItemsMass()/weightNatron),1)
elseif useL == 0 then
    massNatron = round(math.ceil(natron.getItemsMass()),1)
end
local percentNatron = math.ceil(((math.ceil((natron.getItemsMass()/weightNatron) - 0.5)/maxNatron)*100))
local statusNatron = oreStatus(percentNatron)


html = [[
<style>
body {
  background-image: url('assets.prod.novaquark.com/58139/a0a98231-07bc-4577-bbc6-2f6b361cc4e1.jpg');
  background-repeat: no-repeat; 
  background-attachment: fixed;
  background-position: center top;
}
</style>
<div class="bootstrap">
<h1 style="
	font-size: 8em;
">Hadez Coperation</h1>
<table 
style="
	margin-top: 10px;
	margin-left: auto;
	margin-right: auto;
	width: 80%;
	font-size: 4em;
">
	</br>
	<tr style="
		width: 100%;
		margin-bottom: 30px;
		background-color: red;
		color: white;
	">
		<th>Ore</th>
		<th>Qty</th>
		<th>Levels</th>
		<th>Status</th>
	<tr>
		<th>Chromite</th>
		<th>]]..massChromite..unitMeasure..[[</th>
		<th>]]..percentChromite..[[%</th>
		]]..statusChromite..[[
	</tr>
	<tr>
		<th>Limestone</th>
		<th>]]..massLimestone..unitMeasure..[[</th>
		<th>]]..percentLimestone..[[%</th>
		]]..statusLimestone..[[
	</tr>
	<tr>
		<th>Malachite</th>
		<th>]]..massMalachite..unitMeasure..[[</th>
		<th>]]..percentMalachite..[[%</th>
		]]..statusMalachite..[[
	</tr>
	<tr>
		<th>Natron</th>
		<th>]]..massNatron..unitMeasure..[[</th>
		<th>]]..percentNatron..[[%</th>
		]]..statusNatron..[[
	</tr>

</table>
</div>
]]

screen.setHTML(html)